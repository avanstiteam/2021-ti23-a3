#include <stdio.h>

#include "sdkconfig.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_spi_flash.h"
#include "audio_handler.h"
#include "nvs_flash.h"
#include "tone_input.h"
#include "esp_wifi.h"
#include "lcd.h"
#include "sntp2.h"
#include "radio.h"
#include "led.h"




static const char *TAG = "MAIN_PROGRAM";

TaskHandle_t audioHandlerLifeCycle = NULL;

// Init ESP-32 Flash configuration
// Needed for WiFi
void init_flash(void) {
	esp_err_t err = nvs_flash_init();
    if (err == ESP_ERR_NVS_NO_FREE_PAGES) {
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
    }
}

void app_main(void)
{
    init_flash();
    tcpip_adapter_init();



    led_init();
    setup_lcd();
    turn_on_green(0);
    turn_on_red(0);
    initialize_audio();
    //turnOnRadio();
    //switchToQuestionScreen();
    //switchToTimeScreen();


}
