#ifndef AUDIO_HANDLER_H
#define AUDIO_HANDLER_H

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "freertos/queue.h"

#ifdef __cplusplus
extern "C" {
#endif

void initialize_audio(void);
void play_current_time();
void play_new_question();
void await_input_user();

#ifdef __cplusplus
}
#endif

#endif  // AUDIO_HANDLER_H