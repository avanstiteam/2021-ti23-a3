#include <string.h>
#include <sys/time.h>

#include "generic.h"

#include "nvs_flash.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "audio_element.h"
#include "audio_pipeline.h"
#include "audio_event_iface.h"
#include "audio_common.h"
#include "fatfs_stream.h"
#include "i2s_stream.h"
#include "mp3_decoder.h"

#include "esp_peripherals.h"
#include "periph_sdcard.h"
#include "periph_touch.h"
#include "periph_button.h"
#include "periph_wifi.h"
#include "input_key_service.h"

#include "sdcard_list.h"
#include "sdcard_scan.h"

#include "smbus.h"
#include "sntp_sync.h"
#include "talking_clock.h"
#include "talking_sum.h"
#include "tone_input.h"
#include "led.h"

static const char *TAG = "AUDIO_HANDLER";

audio_pipeline_handle_t pipeline, pipeline_input;
audio_element_handle_t i2s_stream_writer, mp3_decoder, fatfs_stream_reader;
audio_event_iface_handle_t evt;
char *url;

void run_audio_task();

void stmp_timesync_event(struct timeval *tv)
{
    ESP_LOGI(TAG, "Notification of a time synchronization event");
	
	time_t now;
    struct tm timeinfo;
    time(&now);
	
	char strftime_buf[64];
	localtime_r(&now, &timeinfo);
    strftime(strftime_buf, sizeof(strftime_buf), "%c", &timeinfo);
    ESP_LOGI(TAG, "The current date/time in Amsterdam is: %s", strftime_buf);
	
	
	talking_clock_fill_queue();
	
}

void timer_1_sec_callback( TimerHandle_t xTimer ){ 
	// Print current time to the screen
	time_t now;
    struct tm timeinfo;
    time(&now);
	
	char strftime_buf[20];
	localtime_r(&now, &timeinfo);
	sprintf(&strftime_buf[0], "%02d:%02d:%02d", timeinfo.tm_hour, timeinfo.tm_min, timeinfo.tm_sec);
	size_t timeSize = strlen(strftime_buf);
	
	
	// Say the time every hour
	if (timeinfo.tm_min == 0 && timeinfo.tm_sec == 0) {
		talking_clock_fill_queue();
		audio_element_set_uri(fatfs_stream_reader, talking_clock_files[TALKING_CLOCK_ITSNOW_INDEX]); // Set first sample
		audio_pipeline_reset_ringbuffer(pipeline);
		audio_pipeline_reset_elements(pipeline);
		audio_pipeline_change_state(pipeline, AEL_STATE_INIT);
		audio_pipeline_run(pipeline);
	}
	
}
//fills the clock queue with the current time set it on the lcd and says it
void play_current_time(){
    talking_clock_fill_queue();
	audio_element_set_uri(fatfs_stream_reader, talking_clock_files[TALKING_CLOCK_ITSNOW_INDEX]); // Set first sample
	audio_pipeline_reset_ringbuffer(pipeline);
	audio_pipeline_reset_elements(pipeline);
	audio_pipeline_change_state(pipeline, AEL_STATE_INIT);
	audio_pipeline_run(pipeline);
}

//fills the sum queue with a sum, shows it on lcd, plays it and turn the green led on
void play_new_question(){
    turn_on_green(0);
    turn_on_red(0);
    talking_sum_fill_queue();
    audio_element_set_uri(fatfs_stream_reader, talking_sum_files[TALKING_SUM_ANNOUNCE_INDEX]);
    audio_pipeline_reset_ringbuffer(pipeline);
    audio_pipeline_reset_elements(pipeline);
    audio_pipeline_change_state(pipeline, AEL_STATE_INIT);
    audio_pipeline_run(pipeline);
}

//keep waiting till the right input of the user is given
void await_input_user(){
    printf( "waiting for input user");
    int correct = 0;
    int element = 0;
    while(!correct){
        // Wait for the correct tone which corresponds to the answer.
        char ans = listen_for_tone();
        vTaskDelay(100/portTICK_PERIOD_MS);
        // If the given answer is the same as the correct answer, then the "congratulations" audio file will be played.
        if(ans == get_correct_ans()){
            turn_on_green(1);
            turn_on_red(0);
            correct = 1;
            audio_pipeline_stop(pipeline_input);
            audio_pipeline_wait_for_stop(pipeline_input);
            url = talking_sum_files[TALKING_SUM_CORRECT_INDEX];
            ESP_LOGI(TAG, "URL: %s", url);
            audio_element_set_uri(fatfs_stream_reader, url);
            audio_pipeline_reset_ringbuffer(pipeline);
            audio_pipeline_reset_elements(pipeline);
            audio_pipeline_change_state(pipeline, AEL_STATE_INIT);
            audio_pipeline_run(pipeline);
        }
                
    }
}

static esp_err_t input_key_service_cb(periph_service_handle_t handle, periph_service_event_t *evt, void *ctx)
{
    /* Handle touch pad events
           to set the clock or the sum
        */
    audio_board_handle_t board_handle = (audio_board_handle_t) ctx;
    int player_volume;
    audio_hal_get_volume(board_handle->audio_hal, &player_volume);

    if (evt->type == INPUT_KEY_SERVICE_ACTION_CLICK_RELEASE) {
        ESP_LOGI(TAG, "[ * ] input key id is %d", (int)evt->data);
        switch ((int)evt->data) {
            case INPUT_KEY_USER_ID_PLAY:
			{
                ESP_LOGI(TAG, "[ * ] [Play] input key event");
                audio_element_state_t el_state = audio_element_get_state(i2s_stream_writer);
                switch (el_state) {
                    case AEL_STATE_INIT :
                        ESP_LOGI(TAG, "[ * ] Starting audio pipeline");
						talking_clock_fill_queue();
						audio_element_set_uri(fatfs_stream_reader, talking_clock_files[TALKING_CLOCK_ITSNOW_INDEX]);
                        audio_pipeline_run(pipeline);
                        break;
                    case AEL_STATE_RUNNING :
                        ESP_LOGI(TAG, "[ * ] Pausing audio pipeline");
                        audio_pipeline_pause(pipeline);
                        break;
                    case AEL_STATE_PAUSED :
                        ESP_LOGI(TAG, "[ * ] Resuming audio pipeline");
						play_current_time();						
                        break;
                    default :
                        ESP_LOGI(TAG, "[ * ] Not supported state %d", el_state);
                }
                break;
			}
            // If the user presses the set button on the lyra-t board the board will ask a new Math question.
            case INPUT_KEY_USER_ID_SET:
			{
                
                ESP_LOGI(TAG, "[ * ] [Set] input key event");
                ESP_LOGI(TAG, "[ * ] Stopped, advancing to the next song");
                vTaskDelete(refresh_time_handle);
                play_new_question();
                break;
			}
            // Increases the volume.
            case INPUT_KEY_USER_ID_VOLUP:
			{
                ESP_LOGI(TAG, "[ * ] [Vol+] input key event");
                player_volume += 10;
                if (player_volume > 100) {
                    player_volume = 100;
                }
                audio_hal_set_volume(board_handle->audio_hal, player_volume);
                ESP_LOGI(TAG, "[ * ] Volume set to %d %%", player_volume);
                break;
			}
            // Decreases the volume.
            case INPUT_KEY_USER_ID_VOLDOWN:
			{
                ESP_LOGI(TAG, "[ * ] [Vol-] input key event");
                player_volume -= 10;
                if (player_volume < 0) {
                    player_volume = 0;
                }
                audio_hal_set_volume(board_handle->audio_hal, player_volume);
                ESP_LOGI(TAG, "[ * ] Volume set to %d %%", player_volume);
                break;
			}
        }
    }

    return ESP_OK;
}

//setup audio pipelines ans start listining for the touch pads events
void initliazise_audio_components(){
    // Setup logging level
    esp_log_level_set("*", ESP_LOG_INFO);
    esp_log_level_set(TAG, ESP_LOG_INFO);

    ESP_LOGI(TAG, "[1.0] Initialize peripherals management");
    esp_periph_config_t periph_cfg = DEFAULT_ESP_PERIPH_SET_CONFIG();
    esp_periph_set_handle_t set = esp_periph_set_init(&periph_cfg);

    ESP_LOGI(TAG, "[1.1] Initialize and start peripherals");
    audio_board_key_init(set);
    audio_board_sdcard_init(set,SD_MODE_1_LINE);

	
    ESP_LOGI(TAG, "[ 2 ] Start codec chip");
    audio_board_handle_t board_handle = audio_board_init();
    audio_hal_ctrl_codec(board_handle->audio_hal, AUDIO_HAL_CODEC_MODE_DECODE, AUDIO_HAL_CTRL_START);
	
	ESP_LOGI(TAG, "[ 3 ] Start and wait for Wi-Fi network");
    periph_wifi_cfg_t wifi_cfg = {
        .ssid = CONFIG_WIFI_SSID,
        .password = CONFIG_WIFI_PASSWORD,
    };

    esp_periph_handle_t wifi_handle = periph_wifi_init(&wifi_cfg);
    esp_periph_start(set, wifi_handle);
    periph_wifi_wait_for_connected(wifi_handle, portMAX_DELAY);

    ESP_LOGI(TAG, "[ 3 ] Create and start input key service");
    input_key_service_info_t input_key_info[] = INPUT_KEY_DEFAULT_INFO();
    input_key_service_cfg_t input_cfg = INPUT_KEY_SERVICE_DEFAULT_CONFIG();
    input_cfg.handle = set;
    periph_service_handle_t input_ser = input_key_service_create(&input_cfg);
    input_key_service_add_key(input_ser, input_key_info, INPUT_KEY_NUM);
    periph_service_set_callback(input_ser, input_key_service_cb, (void *)board_handle);

    ESP_LOGI(TAG, "[4.0] Create audio pipeline for playback");
    audio_pipeline_cfg_t pipeline_cfg = DEFAULT_AUDIO_PIPELINE_CONFIG();
    pipeline = audio_pipeline_init(&pipeline_cfg);
    pipeline_input = audio_pipeline_init(&pipeline_cfg);

    ESP_LOGI(TAG, "[4.1] Create i2s stream to write data to codec chip");
    i2s_stream_cfg_t i2s_cfg = I2S_STREAM_CFG_DEFAULT();
    i2s_cfg.i2s_config.sample_rate = 48000;
    i2s_cfg.type = AUDIO_STREAM_WRITER;
    i2s_stream_writer = i2s_stream_init(&i2s_cfg);

    ESP_LOGI(TAG, "[4.2] Create mp3 decoder to decode mp3 file");
    mp3_decoder_cfg_t mp3_cfg = DEFAULT_MP3_DECODER_CONFIG();
    mp3_decoder = mp3_decoder_init(&mp3_cfg);

    ESP_LOGI(TAG, "[4.4] Create fatfs stream to read data from sdcard");
    fatfs_stream_cfg_t fatfs_cfg = FATFS_STREAM_CFG_DEFAULT();
    fatfs_cfg.type = AUDIO_STREAM_READER;
    fatfs_stream_reader = fatfs_stream_init(&fatfs_cfg);
    audio_element_set_uri(fatfs_stream_reader, url);

    //set up the pipeline for oudio intput
    audio_input_element(pipeline_input);

    ESP_LOGI(TAG, "[4.5] Register all elements to audio pipeline");
    audio_pipeline_register(pipeline, fatfs_stream_reader, "file");
    audio_pipeline_register(pipeline, mp3_decoder, "mp3");
    audio_pipeline_register(pipeline, i2s_stream_writer, "i2s");

    ESP_LOGI(TAG, "[4.6] Link it together [sdcard]-->fatfs_stream-->mp3_decoder-->i2s_stream-->[codec_chip]");
    audio_pipeline_link(pipeline, (const char *[]) {"file", "mp3", "i2s"}, 3);
    const char *link_tag[3] = {"i2sr", "rsp_filter", "raw"};
    audio_pipeline_link(pipeline_input, &link_tag[0], 3);

    ESP_LOGI(TAG, "[5.0] Set up  event listener");
    audio_event_iface_cfg_t evt_cfg = AUDIO_EVENT_IFACE_DEFAULT_CFG();
    evt = audio_event_iface_init(&evt_cfg);

    ESP_LOGI(TAG, "[5.1] Listen for all pipeline events");
    audio_pipeline_set_listener(pipeline, evt);

    ESP_LOGW(TAG, "[ 6 ] Press the keys to control talking clock");
    ESP_LOGW(TAG, "      [Play] to start reading time");
    ESP_LOGW(TAG, "      [Vol-] or [Vol+] to adjust volume.");
	
	
	// Initialize Talking clock and talking sum method
	talking_clock_init();
    talking_sum_init();
	
	// Synchronize NTP time
	sntp_sync(stmp_timesync_event);
	
	// Setup first audio sample 'it's now'
	audio_element_set_uri(fatfs_stream_reader, talking_clock_files[TALKING_CLOCK_ITSNOW_INDEX]);

    //create audio task for output of the audio queues
    xTaskCreate(run_audio_task,"Audio_task",4096,NULL,4,NULL);
}

void run_audio_task(void *pvParameters){
    while (1) {
        /* Handle event interface messages from pipeline
           to play all the items in the queues
        */
        audio_event_iface_msg_t msg;
        esp_err_t ret = audio_event_iface_listen(evt, &msg, portMAX_DELAY);
        if (ret != ESP_OK) {
            ESP_LOGE(TAG, "[ * ] Event interface error : %d", ret);
            continue;
        }
        if (msg.source_type == AUDIO_ELEMENT_TYPE_ELEMENT) {
            // Set music info for a new song to be played
            if (msg.source == (void *) mp3_decoder
                && msg.cmd == AEL_MSG_CMD_REPORT_MUSIC_INFO) {
                audio_element_info_t music_info = {};
                audio_element_getinfo(mp3_decoder, &music_info);
                ESP_LOGI(TAG, "[ * ] Received music info from mp3 decoder, sample_rates=%d, bits=%d, ch=%d",
                         music_info.sample_rates, music_info.bits, music_info.channels);
                audio_element_setinfo(i2s_stream_writer, &music_info);
                continue;
            }
            // Advance to the next song when previous finishes
            if (msg.source == (void *) i2s_stream_writer
                && msg.cmd == AEL_MSG_CMD_REPORT_STATUS) {
                audio_element_state_t el_state = audio_element_get_state(i2s_stream_writer);
                int element = 0;
                if (el_state == AEL_STATE_FINISHED) {
                    if(uxQueueMessagesWaiting(talking_sum_queue) > 0 && 
						xQueueReceive(talking_sum_queue, &element, portMAX_DELAY)){

                        ESP_LOGI(TAG, "Finish sample sum, towards next sample");
						url = talking_sum_files[element];
						ESP_LOGI(TAG, "URL: %s", url);
						audio_element_set_uri(fatfs_stream_reader, url);
						audio_pipeline_reset_ringbuffer(pipeline);
						audio_pipeline_reset_elements(pipeline);
						audio_pipeline_change_state(pipeline, AEL_STATE_INIT);
						audio_pipeline_run(pipeline);

                         if(uxQueueMessagesWaiting(talking_sum_queue) == 0){
                            audio_pipeline_stop(pipeline);
                            audio_pipeline_wait_for_stop(pipeline);
                            const char *link_tag[3] = {"i2sr", "rsp_filter", "raw"};
                            audio_pipeline_link(pipeline_input, &link_tag[0], 3);
                            audio_pipeline_run(pipeline_input);
                            await_input_user();
 
                         }
                    }else if (uxQueueMessagesWaiting(talking_clock_queue) > 0 && 
						xQueueReceive(talking_clock_queue, &element, portMAX_DELAY)) {

						ESP_LOGI(TAG, "Finish sample clock, towards next sample");
						url = talking_clock_files[element];
						ESP_LOGI(TAG, "URL: %s", url);
						audio_element_set_uri(fatfs_stream_reader, url);
						audio_pipeline_reset_ringbuffer(pipeline);
						audio_pipeline_reset_elements(pipeline);
						audio_pipeline_change_state(pipeline, AEL_STATE_INIT);
						audio_pipeline_run(pipeline);

					} else {
						// No more samples. Pause for now
						audio_pipeline_pause(pipeline);
					}
                }
            }
        }
    }
}


void initialize_audio(void)
{	
    initliazise_audio_components();
}
