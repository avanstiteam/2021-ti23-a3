#include <string.h>
#include <time.h>
	
#include "talking_clock.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_event.h"
#include "esp_log.h"
#include "esp_attr.h"
#include "lcd.h"

static const char *TAG = "TALKING_CLOCK";

// Generates a new queue which will be used for the audio files.
esp_err_t talking_clock_init() {
	
	// Initialize queue
	ESP_LOGI(TAG, "Creating FreeRTOS queue for talking clock");
	talking_clock_queue = xQueueCreate( 10, sizeof( int ) );
	
	if (talking_clock_queue == NULL) {
		ESP_LOGE(TAG, "Error creating queue");
		return ESP_FAIL;
	}
	
	return ESP_OK;
}

// Fills the talking sum queue with the index's of the corrosponding audio files for the generated question.
esp_err_t talking_clock_fill_queue() {
	
	time_t now;
    struct tm timeinfo;
    time(&now);
	
	char strftime_buf[64];
	localtime_r(&now, &timeinfo);
    strftime(strftime_buf, sizeof(strftime_buf), "%c", &timeinfo);
    ESP_LOGI(TAG, "The current date/time in Amsterdam is: %s", strftime_buf);
	
	clear_LCD();
    write_string_position(strftime_buf, 1, 1);
	
	// Reset queue
	esp_err_t ret = xQueueReset(talking_clock_queue);
	if (ret != ESP_OK) {
		ESP_LOGE(TAG, "Cannot reset queue");
	}
	
	int data = TALKING_CLOCK_ITSNOW_INDEX;

	// Convert hours to AM/PM unit
	int hour = timeinfo.tm_hour;
	hour = (hour == 0 ? 12 : hour);
	hour = (hour > 12 ? hour%12 : hour);
	data = TALKING_CLOCK_1_INDEX-1+hour;
	ret = xQueueSend(talking_clock_queue, &data, portMAX_DELAY);
	if (ret != ESP_OK) {
		ESP_LOGE(TAG, "Cannot queue data");
	}
	data = TALKING_CLOCK_HOUR_INDEX;
	ret = xQueueSend(talking_clock_queue, &data, portMAX_DELAY);
	if (ret != ESP_OK) {
		ESP_LOGE(TAG, "Cannot queue data");
	}


	int minute = timeinfo.tm_min;
	//checks if minute is above 20(there need to be more audio files for numbers above 20)
	if(minute > 20){
		data = (TALKING_CLOCK_1_INDEX-1)+(minute%10);
		if(minute%10 != 0){
		ret = xQueueSend(talking_clock_queue, &data, portMAX_DELAY);
		minute = minute - minute%10;
		printf("DATA VAN DE TIENTALLEN = %d",data);
		data = TALKING_CLOCK_AND_INDEX;
		ret = xQueueSend(talking_clock_queue, &data, portMAX_DELAY);
		}
		
		data = (TALKING_CLOCK_20_INDEX-2)+(minute/10);
		ret = xQueueSend(talking_clock_queue, &data, portMAX_DELAY);
		

	}else if(minute<20 && minute > 0){
		data = TALKING_CLOCK_1_INDEX-1+minute;
		ret = xQueueSend(talking_clock_queue, &data, portMAX_DELAY);
	}
	
	ESP_LOGI(TAG, "Queue filled with %d items", uxQueueMessagesWaiting(talking_clock_queue));

	xTaskCreate(refresh_time_task, "refresh_time_task", 4096, NULL, 2, &refresh_time_handle);

	
	return ESP_OK;
}

//refreshes the lcd every 5 seconds
void refresh_time_task(){
	while(1){
	time_t now;
    struct tm timeinfo;
    time(&now);
	
	char strftime_buf[64];
	localtime_r(&now, &timeinfo);
    strftime(strftime_buf, sizeof(strftime_buf), "%c", &timeinfo);
    ESP_LOGI(TAG, "The current date/time in Amsterdam is: %s", strftime_buf);
	
	clear_LCD();
    write_string_position(strftime_buf, 1, 1);
	vTaskDelay(5000 / portTICK_PERIOD_MS);
	}
}


