/*
 * MIT License
 *
 * Copyright (c) 2021 Ben van der Heiden
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdio.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"
#include "driver/i2c.h"
#include "mcp23017.h"
#include "led.h"

//static const char TAG[] = "main";
static mcp23017_info_t *mcp23017_info;
static xSemaphoreHandle semaphore;

#define I2C_MASTER_NUM I2C_NUM_1
#define I2C_MASTER_TX_BUF_LEN 0 // disabled
#define I2C_MASTER_RX_BUF_LEN 0 // disabled
#define I2C_MASTER_FREQ_HZ 100000
#define I2C_MASTER_SDA_IO CONFIG_I2C_MASTER_SDA
#define I2C_MASTER_SCL_IO CONFIG_I2C_MASTER_SCL
#define I2C_ADDRESS CONFIG_MCP23017_I2C_ADDRESS

static void i2c_master_init(void)
{
    i2c_config_t conf;
    conf.mode = I2C_MODE_MASTER;
    conf.sda_io_num = I2C_MASTER_SDA_IO;
    conf.sda_pullup_en = GPIO_PULLUP_ENABLE;
    conf.scl_io_num = I2C_MASTER_SCL_IO;
    conf.scl_pullup_en = GPIO_PULLUP_ENABLE;
    conf.master.clk_speed = I2C_MASTER_FREQ_HZ;
    i2c_param_config(I2C_MASTER_NUM, &conf);
    i2c_driver_install(I2C_MASTER_NUM, conf.mode, I2C_MASTER_RX_BUF_LEN, I2C_MASTER_TX_BUF_LEN, 0);
}

//Method to turn red LED on or off
    void turn_on_red(int on){
    ESP_ERROR_CHECK(mcp23017_pin_write(mcp23017_info, PORTA, 7, on));
}

//Method to turn green LED on or off
void turn_on_green(int on){
    ESP_ERROR_CHECK(mcp23017_pin_write(mcp23017_info, PORTA, 6, on));
}

//Led initialize
void led_init()
{
    i2c_master_init();

    semaphore = xSemaphoreCreateMutex();
    xSemaphoreGive(semaphore);

    mcp23017_info = mcp23017_malloc();
    ESP_ERROR_CHECK(mcp23017_init(mcp23017_info, I2C_MASTER_NUM, I2C_ADDRESS, &semaphore));

    ESP_ERROR_CHECK(mcp23017_port_set_interrupt(mcp23017_info, PORTA, 0x00));
    ESP_ERROR_CHECK(mcp23017_port_set_direction(mcp23017_info, PORTA, 0x0F));
    ESP_ERROR_CHECK(mcp23017_port_set_latch(mcp23017_info, PORTA, 0xFF));
    ESP_ERROR_CHECK(mcp23017_port_set_polarity(mcp23017_info, PORTA, 0x0F));
}
