#define servo_driver_TAG "Servo Motor"

void servo_init();
void emergency_stop();
void set_speed(int speed);
void set_turnrate(float turnrate);

void servo_test();