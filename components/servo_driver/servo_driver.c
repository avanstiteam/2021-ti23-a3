#include <stdio.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <pca9685.h>
#include <string.h>
#include <esp_log.h>

#include "servo_driver.h"

#define ADDR PCA9685_ADDR_BASE
#define SERVO_TAG "SERVO_DRIVER"

#define SDA_GPIO 18
#define SCL_GPIO 23

// 20 ms per signal
#define PWM_FREQ_HZ 45

#define MAX_SPEED 10
#define BASE_FREQ 450
#define STEP_SIZE 1

int current_speed;
int target_speed;
int turn_rate;
int emergency;
QueueHandle_t controllerQueue;
i2c_dev_t device;

enum dataType{
    Speed,
    Turnrate,
    Emergency
}; 

typedef struct dataObject_t{
    int data;
    enum dataType type;
} DataObject;


void update_servo(int channel){
    
    uint16_t frequency;
    if (channel == 0)
    {
        if (turn_rate>0)
        {
            float turn = 1-turn_rate;
            frequency = (current_speed*turn)+BASE_FREQ;
        }else{
            frequency = current_speed+BASE_FREQ;
        }
        
    }else{
        if (turn_rate<0)
        {
            float turn = 1+turn_rate;
            frequency = -(current_speed*turn)+BASE_FREQ;
        }else{
            frequency = -current_speed+BASE_FREQ;
        }
    }
    
    ESP_ERROR_CHECK(pca9685_set_pwm_value(&device,channel,frequency));
}

void update_motor(void *pvParameters){
    for(;;){
        
        // Set all target values
        while (uxQueueMessagesWaiting(controllerQueue)!=0)
        {
            DataObject object;
            xQueueReceive(controllerQueue,(void *) &object,10/portTICK_PERIOD_MS);

            switch (object.type)
            {
            case Speed:
                target_speed = object.data;
                break;
            case Turnrate:
                turn_rate = object.data;
                break;
            case Emergency:
                emergency = object.data;
                break;
            default:
                break;
            }
        }

        // If emergency we imidiatly set the motors still
        if (emergency){
            target_speed = 0;
            current_speed = BASE_FREQ;

            update_servo(0);
            update_servo(1);
            emergency = 0;

        }else{
            // Target is higher then current
            if (target_speed>current_speed)
            {
                // If one step is more then the max
                if(current_speed+STEP_SIZE>target_speed){
                    current_speed = target_speed;
                }else{
                    current_speed+=STEP_SIZE;
                }
            }else{
                // If one step is more then the max
                if(current_speed-STEP_SIZE<target_speed){
                    current_speed = target_speed;
                }else{
                    current_speed-=STEP_SIZE;
                }
        }
        
            update_servo(0);
            update_servo(1);
        }

        vTaskDelay(250/portTICK_PERIOD_MS);
    }
}

// Set servo speed, speed must be between -10 and 10.
void set_speed(int speed){
    // Speed must be within boundries
    if (speed>MAX_SPEED&&speed<-MAX_SPEED)
    {
        ESP_LOGE(SERVO_TAG, "%s speed too big or small\n", __func__);
    }
    
    DataObject object = {speed,Speed};
    xQueueSendToBack(controllerQueue,(void *) &object,10/portTICK_PERIOD_MS);
}

void set_turnrate(float turnrate){
    // turnrate must be within boundries
    if (turnrate>1&&turnrate<-1)
    {
        ESP_LOGE(SERVO_TAG, "%s turnrate too big or small\n", __func__);
    }

    DataObject object = {turnrate,Turnrate};
    xQueueSendToBack(controllerQueue,(void *) &object,10/portTICK_PERIOD_MS);
}

// Stop servo motors immediately
void emergency_stop(){
    DataObject object = {1,Emergency};
    xQueueSendToBack(controllerQueue,(void *) &object,100/portTICK_PERIOD_MS);

    set_speed(0);
    update_motor(NULL);
}

// Initialise servos
void servo_init(){
    ESP_ERROR_CHECK(i2cdev_init());
 
    memset(&device, 0, sizeof(i2c_dev_t));


    ESP_ERROR_CHECK(pca9685_init_desc(&device, ADDR, 0, SDA_GPIO, SCL_GPIO));
    ESP_ERROR_CHECK(pca9685_init(&device));

    ESP_ERROR_CHECK(pca9685_restart(&device));
    
    // Initialise queues
    controllerQueue = xQueueCreate(10,sizeof(DataObject));

    xTaskCreate(update_motor,"Servo updater",4096,NULL,3,NULL);
    printf("Servo's initialised\n");
    
}

// Test written for testing the servos
void servo_test()
{    
    ets_timer_init();
    
    
    for(int i = 0;i<6;i++){
        set_speed(3);
        vTaskDelay(1000/portTICK_PERIOD_MS);
        set_speed(-3);
        vTaskDelay(1000/portTICK_PERIOD_MS);
    }

    for (int i = 0; i < 6; i++)
    {
        set_speed(i);
        set_turnrate(1/6*i);
        vTaskDelay(1000/portTICK_PERIOD_MS);
    }

        printf("Servo's testing done\n");

}

