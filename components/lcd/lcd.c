/**
 * @file lcd.c
 * @brief LCD driver
 */
#include <stdio.h>
#include <stdlib.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "driver/gpio.h"
#include "driver/i2c.h"
#include "esp_log.h"
#include "sdkconfig.h"
#include "esp32/rom/uart.h"
#include "string.h"

#include "lcd.h"
#include "i2c-lcd1602.h"
#include "smbus.h"

#include "sntp2.h"
#include "time.h"
#include "i2c-lcd1602.h"
#include "lcd.h"
#include "smbus.h"
#include "nvs_flash.h"
#include "esp_sntp.h"

#include "audio_handler.h"


#define TAG "app"
#undef USE_STDIN

#define I2C_MASTER_NUM           I2C_NUM_0
#define I2C_MASTER_TX_BUF_LEN    0                    
#define I2C_MASTER_RX_BUF_LEN    0                     
#define I2C_MASTER_FREQ_HZ       100000
#define I2C_MASTER_SDA_IO        18
#define I2C_MASTER_SCL_IO        23
#define LCD_NUM_ROWS			 4
#define LCD_NUM_COLUMNS			 40
#define LCD_NUM_VIS_COLUMNS		 20

i2c_lcd1602_info_t *lcd_info;

//Standard question and answers
char currentQuestion[20] = "0 + 0 = ?";
int currentAnswerA = 0,currentAnswerB = 0,currentAnswerC = 0;

static void i2c_master_init(void)
{
    int i2c_master_port = I2C_MASTER_NUM;
    i2c_config_t conf;
    conf.mode = I2C_MODE_MASTER;
    conf.sda_io_num = I2C_MASTER_SDA_IO;
    conf.sda_pullup_en = GPIO_PULLUP_DISABLE; 
    conf.scl_io_num = I2C_MASTER_SCL_IO;
    conf.scl_pullup_en = GPIO_PULLUP_DISABLE;  
    conf.master.clk_speed = I2C_MASTER_FREQ_HZ;
    i2c_param_config(i2c_master_port, &conf);
    i2c_driver_install(i2c_master_port, conf.mode,
                       I2C_MASTER_RX_BUF_LEN,
                       I2C_MASTER_TX_BUF_LEN, 0);
}

static uint8_t _wait_for_user(void)
{
    uint8_t c = 0;
#ifdef USE_STDIN
    while (!c)
    {
       STATUS s = uart_rx_one_char(&c);
       if (s == OK) {
          printf("%c", c);
       }
    }
#else
    vTaskDelay(1000 / portTICK_RATE_MS);
#endif
    return c;
}

//Setup for LCD
void setup_lcd()
{
    i2c_master_init();
    i2c_port_t i2c_num = I2C_MASTER_NUM;
    uint8_t address = 0x27;

    smbus_info_t * smbus_info = smbus_malloc();
    smbus_init(smbus_info, i2c_num, address);
    smbus_set_timeout(smbus_info, 1000 / portTICK_RATE_MS);

    lcd_info = i2c_lcd1602_malloc();
    i2c_lcd1602_init(lcd_info, smbus_info, true, LCD_NUM_ROWS, LCD_NUM_COLUMNS, LCD_NUM_VIS_COLUMNS);
    i2c_lcd1602_set_backlight(lcd_info, true);
}

//Method to write a string on the LCD on a specific coordinate
void write_string_position(char text[], int x,int y) {
    _wait_for_user();
    i2c_lcd1602_home(lcd_info);
    //i2c_lcd1602_clear(lcd_info);
    i2c_lcd1602_move_cursor(lcd_info, x, y);
    i2c_lcd1602_write_string(lcd_info,text);
}

//Method to write a new question on the LCD
void write_question(char question[], int answerA, int answerB, int answerC){
     i2c_lcd1602_clear(lcd_info);
     strcpy(currentQuestion,question);
     currentAnswerA = answerA;
     currentAnswerB = answerB;
     currentAnswerC = answerC;

     char answers[25];
     sprintf(answers,"A: %d B: %d C: %d",currentAnswerA,currentAnswerB,currentAnswerC);

     write_string_position(currentQuestion,4,1);
     write_string_position(answers,2,3);
}

//Turn on radio screen
void radio_screen(){
    clear_LCD();
    write_string_position("RADIO 538",4,1);
}
//Method to clear the LCD
void clear_LCD(){
        i2c_lcd1602_clear(lcd_info); 
}

//Method to switch to the question screen on LCD
void switch_to_question_screen(){
    play_new_question();
    //await_input_user();
    //writeQuestion(currentQuestion,currentAnswerA,currentAnswerB,currentAnswerC); 
}



