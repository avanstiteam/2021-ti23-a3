#include "i2c-lcd1602.h"
#include "smbus.h"

#ifndef LCD_H
#define LCD_H
void setup_lcd();
void write_string_position(char text[], int x,int y);
void switch_to_Question_screen();
void switch_to_time_screen();
void write_question(char question[], int answerA, int answerB, int answerC);
void clear_LCD();
void radio_screen();

#endif