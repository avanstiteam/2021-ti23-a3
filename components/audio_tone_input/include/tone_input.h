#include "audio_common.h"
#include "audio_pipeline.h"

#ifndef TONE_INPUT_H
#define TONE_INPUT_H

#define TONE_ANSWER_A 880
#define TONE_ANSWER_B 988
#define TONE_ANSWER_C 1047

char listen_for_tone(void);
void audio_input_element(audio_pipeline_handle_t pipeline);

#endif