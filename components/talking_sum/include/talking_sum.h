#ifndef TALKING_SUM_H
#define TALKING_SUM_H

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "freertos/queue.h"

#ifdef __cplusplus
extern "C" {
#endif

#define TALKING_SUM_MAX_STRING 40
#define TALKING_SUM_ITEMS 35

#define TALKING_SUM_MIN_INDEX 0
#define TALKING_SUM_PLUS_INDEX 1
#define TALKING_SUM_IS_INDEX 2
#define TALKING_SUM_A_INDEX 3
#define TALKING_SUM_B_INDEX 4
#define TALKING_SUM_C_INDEX 5

#define TALKING_SUM_0_INDEX 6
#define TALKING_SUM_1_INDEX 7
#define TALKING_SUM_2_INDEX 8
#define TALKING_SUM_3_INDEX 9
#define TALKING_SUM_4_INDEX 10
#define TALKING_SUM_5_INDEX 11
#define TALKING_SUM_6_INDEX 12
#define TALKING_SUM_7_INDEX 13
#define TALKING_SUM_8_INDEX 14
#define TALKING_SUM_9_INDEX 15
#define TALKING_SUM_10_INDEX 16
#define TALKING_SUM_11_INDEX 17
#define TALKING_SUM_12_INDEX 18
#define TALKING_SUM_13_INDEX 19
#define TALKING_SUM_14_INDEX 20
#define TALKING_SUM_15_INDEX 21
#define TALKING_SUM_16_INDEX 22
#define TALKING_SUM_17_INDEX 23
#define TALKING_SUM_18_INDEX 24
#define TALKING_SUM_19_INDEX 25

#define TALKING_SUM_20_INDEX 26
#define TALKING_SUM_30_INDEX 27
#define TALKING_SUM_40_INDEX 28
#define TALKING_SUM_50_INDEX 29
#define TALKING_SUM_60_INDEX 30

#define TALKING_SUM_AND_INDEX 31
#define TALKING_SUM_ANNOUNCE_INDEX 32
#define TALKING_SUM_WRONG_INDEX 33
#define TALKING_SUM_CORRECT_INDEX 34




static char talking_sum_files[TALKING_SUM_ITEMS][TALKING_SUM_MAX_STRING] = {
    "/sdcard/min.mp3",
    "/sdcard/plus.mp3",
    "/sdcard/is.mp3",
    "/sdcard/a.mp3",
    "/sdcard/b.mp3",
    "/sdcard/c.mp3",
    "/sdcard/0.mp3",
	"/sdcard/1.mp3",
	"/sdcard/2.mp3",
	"/sdcard/3.mp3",
	"/sdcard/4.mp3",
	"/sdcard/5.mp3",
	"/sdcard/6.mp3",
	"/sdcard/7.mp3",
	"/sdcard/8.mp3",
	"/sdcard/9.mp3",
	"/sdcard/10.mp3",
	"/sdcard/11.mp3",
	"/sdcard/12.mp3",
	"/sdcard/13.mp3",
	"/sdcard/14.mp3",
	"/sdcard/15.mp3",
	"/sdcard/16.mp3",
	"/sdcard/17.mp3",
	"/sdcard/18.mp3",
	"/sdcard/19.mp3",
	"/sdcard/20.mp3",
	"/sdcard/30.mp3",
	"/sdcard/40.mp3",
	"/sdcard/50.mp3",
    "/sdcard/60.mp3",
    "/sdcard/and.mp3",
	"/sdcard/announce.mp3",
	"/sdcard/wrong.mp3",
	"/sdcard/correct.mp3",
	
};


QueueHandle_t talking_sum_queue;

esp_err_t talking_sum_init();
esp_err_t talking_sum_fill_queue();
char get_correct_ans();

#ifdef __cplusplus
}
#endif

#endif
