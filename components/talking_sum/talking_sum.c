#include <string.h>
#include <time.h>
	
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_event.h"
#include "esp_log.h"
#include "esp_attr.h"

#include "talking_sum.h"
#include "lcd.h"


// Define made for generating a specific random value between 0... MAXRAND's value.
#define MAXRAND 60
#define RANDOM (rand() % MAXRAND + 1)

static const char *TAG = "TALKING_SUM";

esp_err_t ret;
int data;
static int answer;
int answers[3];

// Shuffles the items in the array so that the correct answer is not always on the same position.
void shuffle(int *array, size_t n){
    if (n > 1) 
    {
        size_t i;
        for (i = 0; i < n - 1; i++) 
        {
          size_t j = i + rand() / (RAND_MAX / (n - i) + 1);
          int t = array[j];
          array[j] = array[i];
          array[i] = t;
        }
    }
}

//Returns the character belonging to the correct answer of the question.
char get_correct_ans(){
	static char ans = 'O';
	if(answer == answers[0]){
		ans ='A';
	}else if(answer == answers[1]){
		ans = 'B';
	}else if(answer == answers[2]){
		ans = 'C';
	}

	return ans;

}

//makes sure every answer is different.
void correct_anwser_array(int *array){
		
		while (array[1] == array[0] || array[1] == array[2] || array[2] == array[0] )
		{
			array[1] = RANDOM;
			array[2] = RANDOM;
		}
}

//checks if there need to be multiple audio files for a number
void number_sound(int number){
	if(number>20){
		data = (TALKING_SUM_0_INDEX)+(number%10);
		if(number%10 != 0){
		ret = xQueueSend(talking_sum_queue, &data, portMAX_DELAY);
		number = number - number%10;
		data = TALKING_SUM_AND_INDEX;
		ret = xQueueSend(talking_sum_queue, &data, portMAX_DELAY);
		}
		data = (TALKING_SUM_20_INDEX-2)+(number/10);
		ret = xQueueSend(talking_sum_queue, &data, portMAX_DELAY);
	}else if(number<20 && number >= 0){
		data = TALKING_SUM_0_INDEX+number;
		ret = xQueueSend(talking_sum_queue, &data, portMAX_DELAY);
	}

}

// Generates a new queue which will be used for the audio files.
esp_err_t talking_sum_init() {
	
	// Initialize queue
	ESP_LOGI(TAG, "Creating FreeRTOS queue for talking sum");
	talking_sum_queue = xQueueCreate( 20, sizeof( int ) );
	
	if (talking_sum_queue == NULL) {
		ESP_LOGE(TAG, "Error creating queue");
		return ESP_FAIL;
	}
	
	return ESP_OK;
}

// Fills the talking sum queue with the index's of the corrosponding audio files for the generated question.
esp_err_t talking_sum_fill_queue() {
	
	// Reset queue
	ret = xQueueReset(talking_sum_queue);
	if (ret != ESP_OK) {
		ESP_LOGE(TAG, "Cannot reset queue");
	}
	
	int x = RANDOM;
	int y = RANDOM;
	int opperator = rand() %2;
	answer = 0;

	// Makes sure there are no negative numbers
	while (y > x)
	{
		y = RANDOM;
	}

	// Makes sure answer isnt greater than 70(no audio files beyond this point)
	while(x + y >= 70){
		x = RANDOM;
		y = RANDOM;

	}
	char question[25];
	
	number_sound(x);

	if(opperator == 1){
	data = TALKING_SUM_PLUS_INDEX;
	ret = xQueueSend(talking_sum_queue, &data, portMAX_DELAY);
	answer = x + y;
	
	sprintf(question,"%d + %d = ?",x,y);
	}else{
	data = TALKING_SUM_MIN_INDEX;
	ret = xQueueSend(talking_sum_queue, &data, portMAX_DELAY);
	answer = x - y;
	sprintf(question,"%d - %d = ?",x,y);
	}
	number_sound(y);

	//create answer array and shuffle it
	answers[0] = answer;
	answers[1] = RANDOM;
	answers[2] = RANDOM;
	shuffle(answers,3);

	correct_anwser_array(answers);
	
	data = TALKING_SUM_IS_INDEX;
	ret = xQueueSend(talking_sum_queue, &data, portMAX_DELAY);

	data = TALKING_SUM_A_INDEX;
	ret = xQueueSend(talking_sum_queue, &data, portMAX_DELAY);
	number_sound(answers[0]);

	data = TALKING_SUM_B_INDEX;
	ret = xQueueSend(talking_sum_queue, &data, portMAX_DELAY);
	number_sound(answers[1]);

	data = TALKING_SUM_C_INDEX;
	ret = xQueueSend(talking_sum_queue, &data, portMAX_DELAY);
	number_sound(answers[2]);
	
	data = TALKING_SUM_CORRECT_INDEX;
	ret = xQueueSend(talking_sum_queue, &data, portMAX_DELAY);
	
	ESP_LOGI(TAG, "Queue filled with %d items", uxQueueMessagesWaiting(talking_sum_queue));
	
	write_question(question, answers[0], answers[1], answers[2]);
	return ESP_OK;
}

